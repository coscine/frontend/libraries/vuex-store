import Vue from 'vue';
import Vuex, { Payload, Store } from 'vuex';
import VuexPersist from 'vuex-persist';

import _user from './modules/_user';
import banner from './modules/banner';
import resourcecontentview from './modules/resourcecontentview';


Vue.use(Vuex);


const vuexLocalStorage = new VuexPersist({
  key: 'coscine.vuex.' + coscine.user.id,
  storage: window.localStorage,
  // defines which modules are stored persistent
  reducer: (state: any) => ({
    banner: state.banner,
    resourcecontentview: state.resourcecontentview,
  }),
});

const store = new Vuex.Store({
  modules: {
    _user,
    banner,
    resourcecontentview,
  },
  plugins: [vuexLocalStorage.plugin],
});

export default store;
