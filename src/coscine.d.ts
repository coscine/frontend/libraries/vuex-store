declare var coscine: {
    authorization: {
        bearer: string,
    },
    language: {
        locale: string,
    },
    user: {
        id: string,
    }
};

declare var _spPageContextInfo: any;