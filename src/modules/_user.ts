const namespaced = true;

const state = {
  userId: coscine.user.id,
  jwt: coscine.authorization.bearer,
};

const mutations = {
};

export default {
  namespaced,
  state,
  mutations,
};
