const namespaced = true;

const state = {
  columns: {},
};

const mutations = {
  storeColumns(state: any, payload: any) {
    state.columns[payload.resourceId] = payload.columns;
  },
};

export default {
  namespaced,
  state,
  mutations,
};
