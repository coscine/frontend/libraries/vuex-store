const namespaced = true;

const state = {
  maintenanceBannerVisible: '',
  pilotBannerVisible: false,
};

const mutations = {
  storeBannerVisibility(state: any, bannerInfo: any) {
    if (bannerInfo.name === 'coscine.pilotBanner') {
      state.pilotBannerVisible = bannerInfo.state;
    }
    else if (bannerInfo.name === 'coscine.maintenanceBanner') {
      state.maintenanceBannerVisible = bannerInfo.state;
    }
  },
};

export default {
  namespaced,
  state,
  mutations,
};
